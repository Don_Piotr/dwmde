# List of key bindings:

## DWM internal:
- Start+[1-9]			- show tag;
- Start+Shift+[1-9]		- show window on tag;
- Start+Ctrl+[1-9]		- show also tag;
- Start+Shift+Ctrl+[1-9]	- show window also on tag;
(0 - for all tags)
- Start+b			- show/hide bar;
- Start+/			- focus next/previous not hidden window/app;
- Start+Shift+/		- focus next/previous (hidden or not) window/app;
- Start+/			- increase/decrease number of master windows;
- Start+Ctrl+/		- move division between master and slave;
- Start+Enter			- move focused window on top of master;
- Start+Tab			- show last tag;
- Start+Shift+q			- quit window/app;
- Start+m/t/f			- switch layout between monocle/tiling/floating;
- Start+s/h			- show/hide window;
- Start+Shift+Space		- make window floating;
- Start+,/.			- focus previous/next monitor;
- Start+Shift+,/.		- move window to previous/next monitor;

## Floating windows
- Start+Left_click		- move windows;
- Start+Shift+Left_click	- resize window;
- Start+Ctrl+Left_click		- switch window floating;
(there are some other mouse bindings, but this are irreplaceable with key binding)

## Media keys
- MonBrightnessDown 		- reduce brightness (only laptop);
- MonBrightnessUp		- raise brightness (only laptop);
- AudioRaiseVolume		- raise volume of default sink;
- AudioLowerVolume		- reduce volume of default sink;
- AudioMute			- mute volume of default sink;

## Run programs:
- Start+d			- run **dmenu**;
- Start+Shift+Enter		- run **terminal with tmux**;
- Start+r			- run **apps menu** (using desktop files);

## DwmDE:
- Start+Escape			- run **DwmDE menu**;
- Start+Shift+Escape		- run **Exit** submenu;
- Start+p			- run dmenu ui for **pass**;
- Start+i			- run **irb**;
- Printscreen			- run **Screenshot** submenu;
- 

## DwmDE switches:
- Start+z			- switch keyboards;
- Start+a			- switch default audio sinks;
- Start+x			- switch touchpad (only laptop);
- Start+Shift+b			- switch long/short bar info;
