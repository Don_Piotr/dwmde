# Config

module DwmDE
  module RunApps
    BLACKLISTED = ["Evolution Alarm Notify","compton","Feh","Access Prompt","View file","GTK+ Demo","Icon Browser",
                   "Widget Factory","i3","Qt V4L2 test Utility","Qt V4L2 video capture utility","Print Preview",
                   "Avahi SSH Server Browser","Avahi Zeroconf Browser","Avahi VNC Server Browser","Web Browser","File Manager",
                   "Preferred Applications","Mail Reader","HP Device Manager","hp-uiscan","Manage Printing","Cheese","About Xfce",
                   "Print Settings","Terminal Emulator","Bulk Rename","Geoclue Demo agent","Keyboard Layout",
                   "PolicyKit Authentication Agent"]
  end

  module Keyboards
    # KBS format: "kb map" => "skb output"
    KBS = {"it" => "Ita", "pl" => "Pol"}
  end

  module FloatWin
    NEWSIZE = [400, 300] # x,y
    BAR_HEIGHT = 18
    TWICEBORDER = 4
  end

  module AppImages
    DIRECTORY = "~/AppImages/"
  end

  module Screens
    OPZIONI = {
        "Laptop + HDMI" => "xrandr --output eDP --primary --mode 1366x768 --pos 0x0 --rotate normal --output HDMI-0 --mode 1280x1024 --pos 1368x0 --rotate normal --output VGA-0 --off",
        "HDMI + Laptop" => "xrandr --output eDP --mode 1366x768 --pos 1280x0 --rotate normal --output HDMI-0 --primary --mode 1280x1024 --pos 0x0 --rotate normal --output VGA-0 --off",
        "Laptop ON" => "xrandr --output eDP --primary --mode 1366x768 --pos 0x0 --rotate normal",
        "Laptop OFF" => "xrandr --output eDP --off",
        "HDMI ON 1280p" => "xrandr --output HDMI-0 --mode 1280x1024 --pos 1368x0 --rotate normal",
        "HDMI ON 1024p" => "xrandr --output HDMI-0 --mode 1024x768 --pos 1368x0 --rotate normal",
        "HDMI OFF" => "xrandr --output HDMI-0 --off",
        "VGA ON 1024p" => "xrandr --output VGA-0 --mode 1280x1024 --pos 1368x0 --rotate normal",
        "VGA ON 768p" => "xrandr --output VGA-0 --mode 1024x768 --pos 1368x0 --rotate normal",
        "VGA OFF" => "xrandr --output VGA-0 --off"}
  end

  module Menus
    MENUS = {
      "menu" => {
        " Applicazioni" => "dwmde run-apps &",
#        " AppImages" => "dwmde appimages &",
#        " Posta" => "dwmde mailboxes &",
        " Volumi" => "x-terminal -e pulsemixer &",
        " Scatta istantanea" => "dwmde printscreen &",
        " Schermi" => "dwmde screens &",
        " Aiuto" => "dwmde help &",
        " Esci/Riavvia" => "menu:exit"
      },
      "exit" => {
        "Esci dwm" => "killall xbindkeys xss-lock dunst unclutter dwmrubybar picom conky dwm-loop",
        "Riavvia dwm" => "killall dwm",
        "----------------" => nil,
        "Riavvia" => "reboot",
        "Spegni" => "poweroff",
        "Sospendi" => "systemctl suspend",
        "Blocca schermo" => "xset s activate",
        "-----------------" => nil,
        "Cambia impostazione salvaschermo" => "dwmde dpms switch"
      }
    }
  end

  module Audio
    AUDIO_UP = "pamixer --allow-boost -i 10"
    AUDIO_DOWN = "pamixer --allow-boost -d 10"
    AUDIO_MUTE = "pamixer -t"
  end

  module Dpms
    DIM_AFTER = 120   # 2 min
    SS_AFTER = 450    # 7,8 min
    # it can't be 10 min, because after 10 min screen get black - and this cause problems
  end

  module Maiboxes
    MB = {"Netc" => "#{ENV["HOME"]}/Mails/config/netc.mutt",
          "Yahoo" => "#{ENV["HOME"]}/Mails/config/yahoo.mutt",
          "Gmail" => "#{ENV["HOME"]}/Mails/config/gmail.mutt",
           "----" => nil,
           "Segretario vescovile" => "#{ENV["HOME"]}/Mails/config/segretario.mutt",
           "Curia vescovile" => "#{ENV["HOME"]}/Mails/config/curia.mutt"}
  end
end

