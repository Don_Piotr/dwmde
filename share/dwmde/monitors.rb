module Monitors
  class Monitor
    attr_reader :name, :x, :y, :w, :h
      def initialize xr_line
      size, @name = xr_line.split[2..3]
      size, @x, @y = size.split("+")
      @x,@y = @x.to_i,@y.to_i
      @w, @h = size.split("x")
      @w = @w.split("/")[0].to_i
      @h = @h.split("/")[0].to_i
    end
    def to_s
      @name +": x=#{@x} y=#{@y} w=#{@w} h=#{@h}"
    end
    def scrot
      system "scrot -d 5 -a #{@x},#{@y},#{@w},#{@h}"
    end
    def is_my_point? x,y
      (@x .. @x+@w).include?(x) && (@y .. @y+@h).include?(y)
    end
    def size
      [@w,@h]
    end
  end # Monitor class
  def self.arr
    @@a
  end
  def self.h
    @@h
  end
  def self.get_data
    @@a = `xrandr --listactivemonitors`.split("\n")[1..-1]
    @@h = {}
    @@a.map!{|mon|
      mon = Monitor.new(mon)
      @@h[mon.to_s] = mon
    }
  end
  def self.which_one x,y
    r = nil
    @@a.each{|m|
      if m.is_my_point?(x,y)
        r = m
        break
      end
    }
    r
  end
end
