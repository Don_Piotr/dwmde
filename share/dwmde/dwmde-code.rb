
module DwmDE
  module Bar
    require "ruby-fifo"
    fifofile = "/tmp/dwmrubybar"
    @@writer = Fifo.new(fifofile, :w, :nowait)
    def self.set m,v=nil
      if v
        @@writer.puts "#{m}:#{v}"
      else
        @@writer.puts m
      end
    end
  end
  module Dmenu
    def self.run arr, nrow=10, p=""
      unless p == ""
        p = " -p \"#{p}\""
      end
      `echo "#{arr.join("\n")}" | dmenu -l #{nrow} -c -fn Terminus:size=15 -i#{p}`.chomp
    end
  end

  module RunApps
    class DesktopFile
      attr_reader :name, :exec, :terminal
      def initialize fn
        @name = nil
        @terminal = nil
        @exec = nil
        f = IO.readlines fn
        f.each {|r|
          if r.include? "="
            r = r.chomp.split "="
            case r[0]
            when "Name"
              @name = r[1] unless @name
            when "Exec"
              @exec = r[1].gsub(/ %./,"") unless @exec
            when "Terminal"
              @terminal = r[1] == "true" ? true : false
            end
          end
        }
        @name = @exec unless @name
        @exec = "x-terminal -e #{@exec}" if @terminal
      end
    end
    @@h = {}
    def self.by_name
      @@h
    end
    def self.get_desktopfiles
      Dir["/usr/share/applications/*.desktop"]+Dir["#{ENV["HOME"]}/.local/share/applications/*.desktop"]
    end

  end

  module Menus
    class MenuItem
      attr_reader :text, :menu, :cmd
      def initialize k,v
        if k =~ /cmd:/
          @text = `#{$'}`.chomp
        else
          @text = k
        end
        if v =~ /^menu:/
          @menu, @cmd = $', nil
        else
          @menu, @cmd = nil, v
        end
      end
    end
    def self.generate_menu h
      @@items_as_objects = {}
      if h.class == Hash
        h.each {|k,v|
          e = MenuItem.new k,v
          @@items_as_objects[e.text] = e
        }
      elsif h.class == Array
        h.each {|k|
          e = MenuItem.new k,k
          @@items_as_objects[e.text] = e
        }
      else
        puts "Errore"
        exit
      end
    end
    def self.menu_keys
      @@items_as_objects.keys - ["default"]
    end
    def self.by_text
      @@items_as_objects
    end
  end
  
  module Monitors
    class Monitor
      attr_reader :name, :x, :y, :w, :h
      def initialize xr_line
        xr_line =~ /\d: \+\*?[\w\-]+ (\d+)\/\d+x(\d+)\/\d+\+(\d+)\+(\d+)\s+([\w\-]+)/
        @w, @h, @x, @y, @name = $1.to_i, $2.to_i, $3.to_i, $4.to_i, $5
      end
      def to_s
        @name +": x=#{@x} y=#{@y} w=#{@w} h=#{@h}"
      end
      def scrot
        system "scrot -d 5 -a #{@x},#{@y},#{@w},#{@h}"
      end
      def is_my_point? x,y
        (@x .. @x+@w).include?(x) && (@y .. @y+@h).include?(y)
      end
      def size
        [@w,@h]
      end
    end # Monitor class
    def self.arr
      @@a
    end
    def self.by_name
      @@h
    end
    def self.get_data
      @@a = `xrandr --listactivemonitors`.split("\n")[1..-1]
      @@h = {}
      @@a.map!{|mon|
        mon = Monitor.new(mon)
        @@h[mon.to_s] = mon
      }
    end
    def self.which_one x,y
      @@a.each{|m|
        return m if m.is_my_point?(x,y)
      }
      nil
    end
  end
  
  module Audio
    class Sink
      attr_reader :id, :name, :mute
      def initialize s
        s = s.split("\n")
        s.map! {|r| r.strip }
        s.each{|r|
          case r
          when /^Sink #(\d+)/
            @id = $1.to_i
          when /Name: (.*)/
            @name = $1
          when /Mute: (\w*)/
            @mute = $1
          end
        }
      end
    end
    def self.get_sinks
      @@sinks = `LC_ALL=C pactl list sinks`.split("\n\n")
      @@sinks_by_name = {}
      @@sinks.each_index {|i|
        @@sinks[i] = Sink.new @@sinks[i]
        @@sinks_by_name[@@sinks[i].name] = i
      }
      @@sinks
    end
    def self.get_default_sink_index
      def_sink_name = nil
      pactl = `LC_ALL=C pactl info`.split("\n")
      pactl.each {|r|
        if r =~ /Default Sink: (.*)/
          def_sink_name = $1
          break
        end
      }
      @@sinks_by_name[def_sink_name]
    end
    def self.get_default_sink
      @@sinks[get_default_sink_index]
    end
  end

  module Dpms
    def self.check_screensaver
      `xset q` =~ /timeout:\s+(\d+)/
      if $1 == "0"
        false
      else
        true
      end
    end
    def self.check_dpms
      `xset q` =~ /DPMS is (\w+)/
      if $1 == "Enabled"
        true
      else
        false
      end
    end
  end
  module Updates
    def self.internet?
      system "ping -c 1 1.1.1.1 1>/dev/null 2>&1"
    end
    def self.check_pkgs
      `yay -Sy`
      s = `checkupdates`
      return nil if s == ""
      s.split("\n").length
    end
    def self.check_kernel
        kern = `pacman -Ss linux-lts | grep "installato:"`
        kern != ""
    end
  end
end


