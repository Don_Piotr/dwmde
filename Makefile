# dwm-scripts
# See LICENSE file for copyright and license details.

VERSION = 1.3

# paths
PREFIX = /usr/local

install:
	chmod 755 ./bin/*
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ./bin/* ${DESTDIR}${PREFIX}/bin/
	mkdir -p ${DESTDIR}${PREFIX}/share
	cp -rf ./share/dwmde ${DESTDIR}${PREFIX}/share

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/checkupdates4dwmrubybar
	rm -f ${DESTDIR}${PREFIX}/bin/dmenu_pass
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-autostart
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-audio
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-appimages
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-backlight
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-bing-wallpaper
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-dim-screen
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-dpms
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-exit
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-float
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-kb-toggle
	rm -f ${DESTDIR}${PREFIX}/bin/dwm-loop
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-run-apps
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-printscreen
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-touchpad
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-screens
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde-starmenu
	rm -f ${DESTDIR}${PREFIX}/bin/dwmde
	rm -rf ${DESTDIR}${PREFIX}/share/dwmde

